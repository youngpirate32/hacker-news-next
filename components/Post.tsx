import Link from 'next/link';
import styled from 'styled-components';
import dateAgo from '../utils/dateAgo';
import { MainItem } from '../api';

const Root = styled.a`
  background-color: #fff;
  padding: 1em;
  box-shadow: 0 4px 3px rgb(0 0 0/0.07), 0 2px 2px rgb(0 0 0/0.06);
  transition: box-shadow .15s cubic-bezier(0,0,.2,1);
  border-radius: 10px;
  display: block;
  text-decoration: none;
  
  &:not(:last-child) {
    margin-bottom: 1em;
  }
  
  &:hover {
    box-shadow: 0 20px 13px rgb(0 0 0/0.03), 0 8px 5px rgb(0 0 0/0.08);
  }
`;

const Title = styled.div`
  margin-bottom: 0.5em;
  font-weight: bold;
`;

const Info = styled.div`
  font-size: 0.8em;
  opacity: 0.6;
`;

function Post({ post }: { post: MainItem }) {
  return (
    <Link href={`/post/${post.id}`} passHref>
      <Root>
        <Title>{post.title}</Title>
        <Info>
          {post.score}
          {' '}
          points by
          {post.by}
        &nbsp;|
          {' '}
          {dateAgo(new Date(post.time * 1000))}
          {'descendants' in post
          && (
          <>
            {' '}
            |
            {post.descendants}
            {' '}
            comments
          </>
          )}
        </Info>
      </Root>
    </Link>
  );
}

export default Post;
