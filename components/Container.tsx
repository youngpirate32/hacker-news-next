import styled from 'styled-components';

const Container = styled.div`
    max-width: 1330px;
    margin: auto;
    padding: 0 15px;
`;

export default Container;
