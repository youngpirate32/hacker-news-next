import Link from 'next/link';
import styled from 'styled-components';
import Container from './Container';

const Root = styled.div`
  background-color: #ff6600;
  margin-bottom: 1em;
  padding: 1em 0;
`;

function Header() {
  return (
    <Root>
      <Container>
        <div>
          <Link href="/"><a>Hacker News</a></Link>
          |
          <Link href="/newest"><a>new</a></Link>
          |
          <Link href="/ask"><a>ask</a></Link>
          |
          <Link href="/show"><a>show</a></Link>
          |
          <Link href="/jobs"><a>jobs</a></Link>
        </div>
      </Container>
    </Root>
  );
}

export default Header;
