import { useState } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import type { Comment as CommentType } from '../api';
import dateAgo from '../utils/dateAgo';

const Root = styled.div`
  margin-bottom: 1.5em;
`;

const Button = styled.button`
  margin-right: 0.5em;
`;

const CommentsRoot = styled.div`
  display: flex;
  align-items: flex-start;
  padding-left: 3em;
  margin-top: 1.5em;
`;

const Comments = styled.div`
`;

const Description = styled.div`
  margin-bottom: 0.5em;
  opacity: 0.5;
`;

function Comment({ comment }: { comment: CommentType }) {
  const [showKids, setShowKids] = useState(true);

  return (
    <Root id={comment.id.toString()}>
      <Description>
        <Link href={`#${comment.id}`}>#</Link>
        {' '}
        {comment.by}
        {' '}
        {dateAgo(new Date(comment.time * 1000))}
      </Description>
      {/* eslint-disable-next-line react/no-danger */}
      <div dangerouslySetInnerHTML={{ __html: comment.text }} />
      {comment.kids && comment.kids.length
        && (
        <CommentsRoot>
          <Button onClick={() => setShowKids(!showKids)}>{showKids ? '-' : '+'}</Button>
          {showKids
            && (
            <Comments>
              {comment.kids && comment.kids.map((kid) => (
                <Comment key={kid.id.toString()} comment={kid} />
              ))}
            </Comments>
            )}
        </CommentsRoot>
        )}
    </Root>
  );
}

export default Comment;
