import axios from 'axios';

const BASE_API_URL = 'https://hacker-news.firebaseio.com/v0';

type ItemType = 'job' | 'story' | 'comment' | 'poll' | 'pollopt';
type StoriesType = 'top' | 'new' | 'best' | 'ask' | 'show' | 'job';
type GetStoriesResponse = number[];
interface ItemBase {
  by: string;
  id: number;
  time: number;
  score: number;
  type: ItemType;
  deleted?: true,
  dead?: true,
}
interface GetComment extends ItemBase {
  type: "comment";
  parent: number;
  text: string;
  kids?: number[];
}
interface GetStory extends ItemBase {
  type: "story";
  descendants: number;
  title: string,
  url: string;
  kids?: number[];
}
interface GetJob extends ItemBase {
  type: "job";
  title: string,
  text: string,
  url: string;
}
interface GetPoll extends ItemBase {
  type: "poll";
  descendants: number;
  kids?: number[];
  parts?: number[];
  title: string,
  text: string,
}
interface GetPollopt extends ItemBase {
  type: "pollopt";
  poll: number,
  text: string,
}

export interface Comment extends Omit<GetComment, 'kids'> {
  kids?: Comment[];
}
export interface Story extends Omit<GetStory, 'kids'> {
  kids?: Comment[];
}
export interface Poll extends Omit<GetPoll, 'kids'> {
  kids?: Comment[];
}

export type GetItem = GetComment|GetStory|GetJob|GetPoll|GetPollopt;
export type Item = Comment|Story|GetJob|Poll|GetPollopt;
export type MainItem = Story|GetJob|Poll;

interface GetItemOverload {
  (id: number, withComments: true): Promise<Item>;
  (id: number, withComments: false): Promise<GetItem>;
}

export const getItem: GetItemOverload = async (id: number, withComments: boolean): Promise<any> => {
  const { data: item } = await axios.get<GetItem>(`${BASE_API_URL}/item/${id}.json`);

  if (withComments && 'kids' in item && item.kids && item.kids.length) {
    const copyItem = {...item} as Comment | Story | Poll;
    copyItem.kids = (await Promise.all(item.kids.map((id) => getItem(id, true)))) as Comment[];

    return copyItem;
  }

  return item;
};

export const getStories = async (type: StoriesType): Promise<MainItem[]> => {
  const { data: storyIds } = await axios.get<GetStoriesResponse>(
    `${BASE_API_URL}/${type}stories.json`
  );

  return await Promise.all(storyIds.slice(0, 30).map(id => getItem(id, false))) as MainItem[];
};
