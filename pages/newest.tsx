import type { NextPage } from 'next';
import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { MainItem, getStories } from '../api';
import Post from '../components/Post';

interface Props {
  posts: MainItem[]
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const posts = await getStories('new');

  return {
    props: {
      posts,
    },
  };
};

const Home: NextPage<Props> = ({ posts }: Props) => (
  <div>
    <Head>
      <title>new</title>
    </Head>
    <div>
      {posts.map((post) => <Post key={post.id.toString()} post={post} />)}
    </div>
  </div>
);

export default Home;
