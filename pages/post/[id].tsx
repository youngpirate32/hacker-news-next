import type { NextPage, GetServerSideProps } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { getItem, Story } from '../../api';
import Comment from '../../components/Comment';

interface Props {
  post: Story;
}

const Root = styled.div`
  background-color: #fff;
  padding: 1px 1em;
`;

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  if (!params || !params.id || Array.isArray(params.id)) {
    return {
      notFound: true,
    };
  }

  const id = parseInt(params.id, 10);

  if (Number.isNaN((id))) {
    return {
      notFound: true,
    };
  }

  const post = await getItem(id, true);

  if (!post) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      post,
    },
  };
};

const PostItem: NextPage<Props> = ({ post }: Props) => (
  <Root>
    <Head>
      <title>{post.title}</title>
    </Head>
    <div>
      <main>
        <h1>
          {post.title}
        </h1>
        {post.kids && post.kids.length
            && (
            <div>
              {post.kids.map((comment) => (
                <Comment key={comment.id.toString()} comment={comment} />
              ))}
            </div>
            )}
      </main>
    </div>
  </Root>
);

export default PostItem;
